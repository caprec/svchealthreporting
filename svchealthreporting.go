package svchealthreporting

import (
	"context"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/google/uuid"
)

type HealthReporter struct {
	host        string
	port        int
	username    string
	password    string
	serviceInfo *ServiceInfo
	heartbeat   *Heartbeat
	Telemetry   *Telemetry
	logger      *log.Logger
}

func NewHealthReporter(host string, port int, username string, password string, serviceInfo ServiceInfo, heartbeat Heartbeat, telemetry Telemetry, logger *log.Logger) *HealthReporter {
	hr := &HealthReporter{
		host:        host,
		port:        port,
		username:    username,
		password:    password,
		serviceInfo: &serviceInfo,
		heartbeat:   &heartbeat,
		Telemetry:   &telemetry,
		logger:      logger,
	}

	return hr
}

type ServiceInfo struct {
	ServiceID uuid.UUID
	Name      string
	Version   string
	HostName  string
	HostIP    string
	Directory string
	Ecosystem string
	Endpoints []SvcEndpoint
	Metadata  []SvcMetadata
}

type SvcEndpoint struct {
	EPType   string
	Protocol string
	IP       string
	Port     int
}

type SvcMetadata struct {
	Key   string
	Value string
}

type Heartbeat struct {
	Interval int64
	Counter  int64
}

func (h *HealthReporter) HeartbeatReporting(ctx context.Context, wg *sync.WaitGroup) error {
	stopreason := new(string)
	defer func(reason *string) {
		if len(*reason) > 0 {
			*reason = fmt.Sprintf(" (%s)", *reason)
		}
		h.logger.Infof("stopping heartbeat reporting service%s", *reason)
		wg.Done()
	}(stopreason)

	h.logger.Infof("starting heartbeat reporting service")
	if h.heartbeat.Interval <= 0 {
		*stopreason = "zero interval specified"
		return nil
	}

	hbTicker := time.NewTicker(time.Duration(h.heartbeat.Interval) * time.Second)
	for {
		select {
		case <-ctx.Done():
			hbTicker.Stop()
			*stopreason = "context closed"
			return ctx.Err()
		case <-hbTicker.C:
			h.heartbeat.Counter++
			if err := h.reportHeartbeat(); err != nil {
				h.logger.WithFields(log.Fields{
					"ServiceID": h.serviceInfo.ServiceID.String(),
					"Cnt":       h.heartbeat.Counter,
				}).Errorf("submitting heartbeat: %v", err)
			}
		}
	}
}

func (h *HealthReporter) TelemetryReporting(ctx context.Context, wg *sync.WaitGroup) error {
	stopreason := new(string)
	defer func(reason *string) {
		if len(*reason) > 0 {
			*reason = fmt.Sprintf(" (%s)", *reason)
		}
		h.logger.Infof("stopping telemetry reporting service%s", *reason)
		wg.Done()
	}(stopreason)

	h.logger.Infof("starting telemetry reporting service")
	if h.Telemetry.duration <= 0 {
		*stopreason = "zero interval specified"
		return nil
	}

	telemTicker := time.NewTicker(time.Duration(h.Telemetry.duration) * time.Second)
	for {
		select {
		case <-ctx.Done():
			telemTicker.Stop()
			*stopreason = "context closed"
			return ctx.Err()
		case <-telemTicker.C:
			// if h.Telemetry.totalRequests == 0 {
			// 	break
			// }
			if err := h.reportTelemetry(); err != nil {
				h.logger.WithFields(log.Fields{
					"ServiceID": h.serviceInfo.ServiceID.String(),
				}).Errorf("submitting telemetry: %v", err)
			} else {
				h.Telemetry.reset()
			}
		}
	}
}

func (h *HealthReporter) RegisterService() error {
	h.logger.WithField("ServiceID", h.serviceInfo.ServiceID).Debug("call REST webservice to register service")
	url := fmt.Sprintf("%s:%d/svcmonhub/register", h.host, h.port)
	method := "POST"

	endpoints := []string{}
	for _, ep := range h.serviceInfo.Endpoints {
		endpoints = append(endpoints, fmt.Sprintf(
			`{
				"Type": "%s",
				"Protocol": "%s",
				"IP": "%s",
				"Port": %d
			}`,
			ep.EPType,
			ep.Protocol,
			ep.IP,
			ep.Port,
		))
	}

	metadata := []string{}
	for _, m := range h.serviceInfo.Metadata {
		metadata = append(metadata, fmt.Sprintf(
			`"%s": "%s"`,
			m.Key,
			m.Value,
		))
	}

	payload := strings.NewReader(fmt.Sprintf(`
		{
			"ServiceID": "%s",
			"Name": "%s",
			"Version": "%s",
			"HostName": "%s",
			"HostIP": "%s",
			"Directory": "%s",
			"Period": "%d",
			"Ecosystem": "%s",
			"Active": true,
			"Endpoints": [
				%s
			],
			"Metadata": {
				%s
			}
		}`,
		strings.ToUpper(h.serviceInfo.ServiceID.String()),
		h.serviceInfo.Name,
		h.serviceInfo.Version,
		h.serviceInfo.HostName,
		h.serviceInfo.HostIP,
		h.serviceInfo.Directory,
		h.heartbeat.Interval,
		h.serviceInfo.Ecosystem,
		strings.Join(endpoints[:], ","),
		strings.Join(metadata[:], ","),
	))

	client := &http.Client{}
	// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
	client.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		DisableKeepAlives: true, // Use the client only for this one connection
	}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return fmt.Errorf("error creating REST webservice to register service: %w", err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(h.username, h.password)

	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error calling REST webservice to register service: %w", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("error reading REST response: %w", err)
	}

	if statusOK := res.StatusCode >= 200 && res.StatusCode < 300; !statusOK {
		return fmt.Errorf(string(body))
	}

	h.logger.Tracef("REST response: %s", string(body))
	return nil
}

func (h *HealthReporter) reportHeartbeat() error {
	h.logger.Tracef("call REST webservice to report heartbeat %d for service %s", h.heartbeat.Counter, h.serviceInfo.ServiceID)
	url := fmt.Sprintf("%s:%d/svcmonhub/%s/heartbeat", h.host, h.port, strings.ToUpper(h.serviceInfo.ServiceID.String()))
	method := "PUT"

	payload := strings.NewReader(fmt.Sprintf(`
		{
			"Counter": %d
		}`, h.heartbeat.Counter))

	client := &http.Client{}
	// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
	client.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		DisableKeepAlives: true, // Use the client only for this one connection
	}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return fmt.Errorf("error creating REST webservice to report heartbeat: %w", err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(h.username, h.password)

	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error calling REST webservice to report heartbeat: %w", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("error reading REST response: %w", err)
	}

	h.logger.Tracef("REST response: %s", string(body))
	return nil
}

func (h *HealthReporter) reportTelemetry() error {
	md := make(map[string]string)
	for _, fn := range h.Telemetry.MetaData {
		m := fn()
		for k, v := range m {
			md[k] = v
		}
	}

	if h.Telemetry.totalRequests == 0 && len(md) == 0 {
		return nil
	}

	h.logger.Tracef("Call REST webservice to send telemetry report for service %s", h.serviceInfo.ServiceID)
	url := fmt.Sprintf("%s:%d/svcmonhub/%s/telemetry", h.host, h.port, strings.ToUpper(h.serviceInfo.ServiceID.String()))
	method := "PUT"

	var mdpayload string
	if len(md) > 0 {
		mdpayload = `,
			"Metadata":[`
		for k, v := range md {
			mdpayload = fmt.Sprintf(`%s
				{"Key":"%s", "Value":"%s"},`, mdpayload, k, v)
		}
		mdpayload = mdpayload[:len(mdpayload)-1] // Remove trailing comma from list items
		mdpayload = fmt.Sprintf(`%s
			]
			`, mdpayload)
	}

	payload := strings.NewReader(fmt.Sprintf(`
		{
			"Timeslot":"%s",
			"Duration":%d,
			"TotalRequests":%d,
			"ErrorCnt":%d,
			"AvgRspTime":%f,
			"MaxRspTime":%f,
			"PeakRspTime":%f,
			"Capacity":%d%s
		}`,
		h.Telemetry.timeslot.Format(time.RFC3339),
		h.Telemetry.duration,
		h.Telemetry.totalRequests,
		h.Telemetry.errorCnt,
		h.Telemetry.avgRspTime,
		h.Telemetry.maxRspTime,
		h.Telemetry.peakRspTime,
		h.Telemetry.capacity,
		mdpayload,
	))

	client := &http.Client{}
	// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
	client.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		DisableKeepAlives: true, // Use the client only for this one connection
	}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return fmt.Errorf("error creating REST webservice to report telemetry: %w", err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error calling REST webservice to report telemetry: %w", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("error reading REST response: %w", err)
	}

	h.logger.Tracef("REST response: %s", string(body))
	return nil
}
