package svchealthreporting

import (
	"math"
	"sync"
	"time"
)

type Telemetry struct {
	timeslot      time.Time
	duration      int64
	totalRequests int32
	errorCnt      int32
	avgRspTime    float64
	maxRspTime    float64
	peakRspTime   float64
	capacity      int32
	MetaData      []GetMetadata
	m             sync.Mutex
}

//GetMetadata is called by the telemetry reporter to obtain service specific metadata
type GetMetadata func() map[string]string

func NewTelemetry(duration int64, metaData ...GetMetadata) Telemetry {
	return Telemetry{
		timeslot:    time.Now(),
		duration:    duration,
		peakRspTime: math.MaxFloat64,
		MetaData:    metaData,
	}
}

func (t *Telemetry) reset() {
	t.m.Lock()
	t.timeslot = time.Now()
	t.totalRequests = 0
	t.errorCnt = 0
	t.avgRspTime = 0
	t.maxRspTime = 0
	t.peakRspTime = math.MaxFloat64
	t.capacity = 0
	t.m.Unlock()
}

func (t *Telemetry) AddReq(rspTime float64, success bool) {
	t.m.Lock()
	t.totalRequests++
	if !success {
		t.errorCnt++
	}
	t.avgRspTime = (t.avgRspTime*float64(t.totalRequests-1) + rspTime) / float64(t.totalRequests)
	if rspTime > t.maxRspTime {
		t.maxRspTime = rspTime
	}
	if rspTime < t.peakRspTime {
		t.peakRspTime = rspTime
	}

	t.m.Unlock()
}
