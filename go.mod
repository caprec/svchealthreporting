module bitbucket.org/caprec/svchealthreporting

go 1.13

require (
	github.com/google/uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
)
