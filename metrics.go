package svchealthreporting

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type Metrics struct {
	host      string
	port      int
	auth      string
	serviceID string
	logger    *log.Logger
}

func NewMetrics(host string, port int, auth string, svcID string, logger *log.Logger) *Metrics {
	return &Metrics{
		host:      host,
		port:      port,
		auth:      auth,
		serviceID: svcID,
		logger:    logger,
	}
}

func (m *Metrics) Counter(name string, increment float64, sampling float64, meta map[string]string) {
	if m == nil {
		return
	}
	go func() {
		uri := fmt.Sprintf("%s:%d/svcmonhub/metrics/counter/%s/%s/%f/%f", m.host, m.port, m.serviceID, name, increment, sampling)
		b := map[string]interface{}{
			"metadata": meta,
		}
		body, err := json.Marshal(b)
		if err != nil {
			m.logger.Errorf("marshalling counter metadata to json: %v", err)
			return
		}
		client := &http.Client{}
		// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DisableKeepAlives: true, // Use the client only for this one connection
		}
		req, err := http.NewRequest("POST", uri, bytes.NewBuffer(body))
		if err != nil {
			m.logger.Errorf("creating request for counter: %v", err)
			return
		}
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization", m.auth)
		rsp, err := client.Do(req)
		if err != nil {
			m.logger.Errorf("posting counter: %v", err)
			return
		}
		defer rsp.Body.Close()
		bodyBytes, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			m.logger.Errorf("reading counter response: %v", err)
			return
		}

		var rb interface{}
		json.Unmarshal(bodyBytes, &rb)

		if rsp.StatusCode != 200 {
			m.logger.Errorf("counter response %s: %s", rsp.Status, rb)
		}
	}()
}

func (m *Metrics) Timer(name string, duration float64, sampling float64, meta map[string]string) {
	if m == nil {
		return
	}
	go func() {
		uri := fmt.Sprintf("%s:%d/svcmonhub/metrics/timer/%s/%s/%f/%f", m.host, m.port, m.serviceID, name, duration, sampling)
		b := map[string]interface{}{
			"metadata": meta,
		}
		body, err := json.Marshal(b)
		if err != nil {
			m.logger.Errorf("marshalling timer metadata to json: %v", err)
			return
		}
		client := &http.Client{}
		// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DisableKeepAlives: true, // Use the client only for this one connection
		}
		req, err := http.NewRequest("POST", uri, bytes.NewBuffer(body))
		if err != nil {
			m.logger.Errorf("creating request for timer: %v", err)
			return
		}
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization", m.auth)
		rsp, err := client.Do(req)
		if err != nil {
			m.logger.Errorf("posting timer: %v", err)
			return
		}
		defer rsp.Body.Close()
		bodyBytes, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			m.logger.Errorf("reading timer response: %v", err)
			return
		}

		var rb interface{}
		json.Unmarshal(bodyBytes, &rb)

		if rsp.StatusCode != 200 {
			m.logger.Errorf("timer response %s: %s", rsp.Status, rb)
		}
	}()
}

func (m *Metrics) GaugeSet(name string, value float64, meta map[string]string) {
	if m == nil {
		return
	}
	go func() {
		uri := fmt.Sprintf("%s:%d/svcmonhub/metrics/gauge/%s/%s/%f", m.host, m.port, m.serviceID, name, value)
		b := map[string]interface{}{
			"metadata": meta,
		}
		body, err := json.Marshal(b)
		if err != nil {
			m.logger.Errorf("marshalling setgauge metadata to json: %v", err)
			return
		}
		client := &http.Client{}
		// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DisableKeepAlives: true, // Use the client only for this one connection
		}
		req, err := http.NewRequest("POST", uri, bytes.NewBuffer(body))
		if err != nil {
			m.logger.Errorf("creating request for setgauge: %v", err)
			return
		}
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization", m.auth)
		rsp, err := client.Do(req)
		if err != nil {
			m.logger.Errorf("posting setgauge: %v", err)
			return
		}
		defer rsp.Body.Close()
		bodyBytes, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			m.logger.Errorf("reading setgauge response: %v", err)
			return
		}

		var rb interface{}
		json.Unmarshal(bodyBytes, &rb)

		if rsp.StatusCode != 200 {
			m.logger.Errorf("setgauge response %s: %s", rsp.Status, rb)
		}
	}()
}

func (m *Metrics) GaugeInc(name string, delta float64, meta map[string]string) {
	if m == nil {
		return
	}
	go func() {
		uri := fmt.Sprintf("%s:%d/svcmonhub/metrics/gauge/%s/%s/%f", m.host, m.port, m.serviceID, name, delta)
		b := map[string]interface{}{
			"metadata": meta,
		}
		body, err := json.Marshal(b)
		if err != nil {
			m.logger.Errorf("marshalling incgauge metadata to json: %v", err)
			return
		}
		client := &http.Client{}
		// The certificate for the SvcMonHub might not contain IP SANs but seeing as comms to SvcMonHub occurs on an internal network, the check can be skipped
		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			DisableKeepAlives: true, // Use the client only for this one connection
		}
		req, err := http.NewRequest("PATCH", uri, bytes.NewBuffer(body))
		if err != nil {
			m.logger.Errorf("creating request for incgauge: %v", err)
			return
		}
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization", m.auth)
		rsp, err := client.Do(req)
		if err != nil {
			m.logger.Errorf("posting incgauge: %v", err)
			return
		}
		defer rsp.Body.Close()
		bodyBytes, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			m.logger.Errorf("reading incgauge response: %v", err)
			return
		}

		var rb interface{}
		json.Unmarshal(bodyBytes, &rb)

		if rsp.StatusCode != 200 {
			m.logger.Errorf("incgauge response %s: %s", rsp.Status, rb)
		}
	}()
}
